package com.dms.printerzebra.data.source;

import com.dms.printerzebra.data.source.local.AppDatabase;
import com.dms.printerzebra.data.source.local.models.Ticket;
import com.dms.printerzebra.injection.annotations.ApplicationScope;
import javax.inject.Inject;
import io.reactivex.Single;

@ApplicationScope
public class DataSourceLocal implements DataSource {

    private AppDatabase appDatabase;

    @Inject
    public DataSourceLocal(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    @Override
    public Single<Ticket> find(int id) {
        return appDatabase.ticketDao().find(id);
    }

    @Override
    public Single<Ticket> last() {
        return appDatabase.ticketDao().last();
    }

    @Override
    public Single<Long> insert(Ticket ticket) {
        return  appDatabase.ticketDao().insert(ticket);
    }
}
