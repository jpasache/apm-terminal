package com.dms.printerzebra.data.source.local.dao;

import android.text.method.SingleLineTransformationMethod;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.dms.printerzebra.data.source.local.models.Ticket;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface TicketDao {

    @Query("UPDATE Ticket SET numero=:numero, fecha=:fecha, peso =:peso, conductor =:conductor, empresa = :empresa WHERE id = :id")
    Completable update(int id, String numero, String fecha, int peso,String conductor,String empresa);

    @Query("SELECT * FROM  Ticket WHERE id = :id")
    Single<Ticket> find(int id);

    @Query("SELECT * FROM  Ticket WHERE id  = (SELECT MAX(id)  FROM Ticket)")
    Single<Ticket> last();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(Ticket ticket);

}
