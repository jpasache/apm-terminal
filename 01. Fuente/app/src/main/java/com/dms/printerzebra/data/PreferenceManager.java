package com.dms.printerzebra.data;

import android.content.Context;
import android.content.SharedPreferences;
import com.dms.printerzebra.injection.annotations.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class PreferenceManager {

    private final SharedPreferences mPreferences;

    @Inject
    public PreferenceManager(@ApplicationScope Context context) {
        mPreferences = context.getSharedPreferences("printer.preferencias", Context.MODE_PRIVATE);
    }


    public String getPrinter1Address() {
        return mPreferences.getString("PREF_PRINTER_ADDRESS", "");
    }

    public void savePrinter1Address(String printer) {
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("PREF_PRINTER_ADDRESS", printer);
        editor.apply();
    }
}
