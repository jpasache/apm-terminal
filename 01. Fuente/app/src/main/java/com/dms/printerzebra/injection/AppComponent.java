package com.dms.printerzebra.injection;

import android.content.Context;

import com.dms.printerzebra.data.PreferenceManager;
import com.dms.printerzebra.data.source.DataSourceRepository;
import com.dms.printerzebra.data.source.local.AppDatabase;
import com.dms.printerzebra.injection.annotations.ApplicationScope;
import com.dms.printerzebra.injection.modules.ContextModule;
import com.dms.printerzebra.injection.modules.SqliteModule;

import dagger.Component;

@ApplicationScope
@Component(modules = { ContextModule.class, SqliteModule.class})
public interface AppComponent {

    PreferenceManager preferenceManager();

    DataSourceRepository dataSourceRepository();

    AppDatabase appDatabase();

    Context context();

}
