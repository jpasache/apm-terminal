package com.dms.printerzebra.feature;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.dms.printerzebra.App;
import com.dms.printerzebra.R;
import com.dms.printerzebra.data.PreferenceManager;
import com.dms.printerzebra.data.source.DataSourceRepository;
import com.dms.printerzebra.data.source.local.models.Ticket;
import com.dms.printerzebra.databinding.ActivityMainBinding;
import com.dms.printerzebra.injection.ActivityComponent;
import com.dms.printerzebra.injection.DaggerActivityComponent;
import com.dms.printerzebra.util.ZebraObservable;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    String TAG = MainActivity.class.getSimpleName();

    private ActivityComponent activityComponent;

    PreferenceManager preferenceManager;
    private static final int REQUEST_CODE = 888;
    private static final int READ_FILE = 668;
    private int idTicket = 0;

    private ActivityMainBinding binding;

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    DataSourceRepository dataSourceRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        activityComponent = DaggerActivityComponent.builder().appComponent(App.get(this).getAppComponent()).build();
        activityComponent.inject(this);

        preferenceManager = new PreferenceManager(this);
        binding.bSeleccionarImpresora.setOnClickListener(this);
        binding.bImportar.setOnClickListener(this);
        binding.bImprimir.setOnClickListener(this);

    }

    private void obtenerInformacion(int id) {
        compositeDisposable.add(
                dataSourceRepository.find(id)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((ticket) -> {
                            Log.e(TAG, "SI" + ticket.getEmpresa());
                            printZebra(ticket);
                        }, throwable -> {
                            Log.e(TAG, "throwable" + throwable);
                        })
        );
    }

    private void obtenerInformacion() {
        compositeDisposable.add(
                dataSourceRepository.last()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((ticket) -> {
                            Log.e(TAG, "SI" + ticket.getEmpresa());
                            printZebra(ticket);
                        }, throwable -> {
                            Log.e(TAG, "throwable" + throwable);
                        })
        );
    }

    private void printZebra(Ticket ticket) {
        String message = "";
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(getAssets().open("VARIAVBLEMODELOAPM03.prn")));

            String mLine;
            while ((mLine = reader.readLine()) != null) {
                message = message + mLine;
            }

            message = message.replace("[CODIGO LINEAL]", ticket.getNumero());
            message = message.replace("[NUMERO DE GUIA]", ticket.getNumero());
            message = message.replace("[FECHADE EMISION]", ticket.getFecha());
            message = message.replace("[PESO DEL PROD]", "" + ticket.getPeso());
            message = message.replace("[NOMBRE DEL CHOFER]", ticket.getConductor());
            message = message.replace("[NOMBRE EMPRESA]", ticket.getEmpresa());

        } catch (IOException e) {
            Log.d(TAG, "printZebra error1: " + e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.d(TAG, "printZebra error2: " + e.getMessage());
                }
            }
        }
        Log.e(TAG, "printZebra message: " + message);

        ZebraObservable.getBluetooth(preferenceManager.getPrinter1Address(), message)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                            Log.e(TAG, "printZebra FIN");
                        }, throwable -> {
                            Log.e(TAG, "printZebra throwable: " + throwable.getMessage());
                            throwable.printStackTrace();
                        }
                );
    }

    private String readText(String path) {
        File file = new File(Environment.getExternalStorageDirectory() + "/" + path);
        Log.d(TAG, "readText file: " + file);
        StringBuilder text = new StringBuilder();
        Log.d(TAG, "readText text init: " + text);
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            Log.d(TAG, "readText text init: " + text);
            String line = "";
            Log.d(TAG, "readText line init: " + line);
            while ((line = br.readLine()) != null) {
                Log.d(TAG, "readText line while: " + line);
                text.append(line);
                Log.d(TAG, "readText text while: " + text);
            }
            br.close();
        } catch (Exception ex) {
            Log.d(TAG, "readText error: " + ex.getMessage());
            ex.printStackTrace();
        }
        Log.d(TAG, "readText text final: " + text.toString());
        return text.toString();
    }

    private void fileSearch() {
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("text/*");
        startActivityForResult(intent, READ_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        if (requestCode == READ_FILE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                Log.d(TAG, "onActivityResult uri: " + uri);
                String path = uri.getPath();
                Log.d(TAG, "onActivityResult path1: " + path);
                path = path.substring(path.indexOf(":") + 1);
                Log.d(TAG, "onActivityResult path2: " + path);
                actualizarDatos(readText(path));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    private void actualizarDatos(String readText) {
        String[] campos = readText.split("\\|");

        Ticket ticket = new Ticket();
        ticket.setNumero(campos[0]);
        ticket.setFecha(campos[1]);
        ticket.setPeso(Integer.parseInt(campos[2]));
        ticket.setConductor(campos[3]);
        ticket.setEmpresa(campos[4]);

        compositeDisposable.add(
                dataSourceRepository.insert(ticket)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe((index) -> {
                            this.idTicket = Integer.parseInt(index.toString());
                            Toast.makeText(MainActivity.this, "Se registro los nuevos datos.", Toast.LENGTH_LONG).show();
                        }, throwable -> {
                            Log.e(TAG, "throwable" + throwable);
                        })
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "PERMISSION_GRANTED YES");
                fileSearch();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.b_seleccionarImpresora:
                Intent intent = new Intent(MainActivity.this, ConfigPrintActivity.class);
                startActivity(intent);
                break;
            case R.id.b_importar:
                if (ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    fileSearch();
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
                }
                break;
            case R.id.b_imprimir:
                if (idTicket > 0) {
                    obtenerInformacion(idTicket);
                } else {
                    obtenerInformacion();
                }
                break;
        }
    }
}
