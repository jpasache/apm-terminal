package com.dms.printerzebra.data.source;

import com.dms.printerzebra.data.source.local.models.Ticket;
import com.dms.printerzebra.injection.annotations.ApplicationScope;
import javax.inject.Inject;
import io.reactivex.Single;

@ApplicationScope
public class DataSourceRepository implements DataSource {

    private final DataSource dataSourceLocal;

    @Inject
    public DataSourceRepository(DataSourceLocal dataSourceLocal) {
        this.dataSourceLocal = dataSourceLocal;
    }

    @Override
    public Single<Ticket> find(int id) {
        return dataSourceLocal.find(id);
    }

    @Override
    public Single<Ticket> last() {
        return dataSourceLocal.last();
    }

    @Override
    public Single<Long> insert(Ticket ticket) {
        return dataSourceLocal.insert(ticket);
    }

}
