package com.dms.printerzebra;

import android.app.Application;
import android.content.Context;

import com.dms.printerzebra.injection.AppComponent;
import com.dms.printerzebra.injection.DaggerAppComponent;
import com.dms.printerzebra.injection.modules.ContextModule;


public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        buildDependecyInjection();
    }


    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }

    public void buildDependecyInjection() {
        appComponent = DaggerAppComponent.builder() .contextModule(new ContextModule(this))  .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

}
