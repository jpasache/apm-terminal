package com.dms.printerzebra.injection;

import com.dms.printerzebra.feature.ConfigPrintActivity;
import com.dms.printerzebra.feature.MainActivity;
import com.dms.printerzebra.injection.annotations.ActivityScope;

import dagger.Component;

/**
 * This component inject dependencies to all Activities across the application
 */
@ActivityScope
@Component( dependencies = AppComponent.class)
public interface ActivityComponent {

//    Activity

    void inject(MainActivity activity);
    void inject(ConfigPrintActivity activity);

//    Fragments
}
