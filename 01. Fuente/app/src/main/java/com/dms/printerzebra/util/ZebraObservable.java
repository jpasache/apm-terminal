package com.dms.printerzebra.util;

import android.widget.Toast;

import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;
import com.zebra.sdk.printer.ZebraPrinterLanguageUnknownException;
import io.reactivex.Completable;

public class ZebraObservable {

    public static Completable getBluetooth(final String address, final String message  ) {
        return Completable.create(e -> {
            Connection printerConnection = new BluetoothConnection(address);
            try {
                printerConnection.open();
                ZebraPrinter printer = null;
                if (printerConnection.isConnected()) {
                    printer = ZebraPrinterFactory.getInstance(printerConnection);
                }else{
                    e.onError(new Throwable("Ocurrio un error al conectar con la impresora"));
                }

                if (printer != null) {
                   PrinterLanguage pl = printer.getPrinterControlLanguage();
                    if (pl == PrinterLanguage.CPCL) {

                    } else if (pl == PrinterLanguage.ZPL) {
                        byte[] configLabel = message.getBytes();
                        printerConnection.write(configLabel);
                    } else if (pl == PrinterLanguage.LINE_PRINT) {

                    }

                    Thread.sleep(500); //Give the printer time to receive the data before closing the connection
                    printerConnection.close();

                } else {
                    printerConnection.close();
                }
                e.onComplete();
            } catch (ConnectionException e1) {
                e.onError(new Throwable("Error al conectar la impresora"));
                printerConnection.close();
            } catch (ZebraPrinterLanguageUnknownException e1) {
                e.onError(new Throwable("Error al conectar la impresora"));
                printerConnection.close();
            }
        });
    }

}
