package com.dms.printerzebra.feature;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dms.printerzebra.App;
import com.dms.printerzebra.R;
import com.dms.printerzebra.data.PreferenceManager;
import com.dms.printerzebra.databinding.ActivityConfigPrintBinding;
import com.dms.printerzebra.injection.ActivityComponent;
import com.dms.printerzebra.injection.DaggerActivityComponent;
import com.dms.printerzebra.util.BluetoothRecyclerAdapter;
import com.dms.printerzebra.util.Constants;
import com.dms.printerzebra.util.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ConfigPrintActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityComponent activityComponent;
    private BluetoothAdapter mBluetoothAdapter;

    private ActivityConfigPrintBinding binding;

    @Inject
    PreferenceManager preferenceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityConfigPrintBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        activityComponent = DaggerActivityComponent.builder().appComponent(App.get(this).getAppComponent()).build();
        activityComponent.inject(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        searchBluetoothDevices();
        binding.btEnable.setOnClickListener(this);
    }

    public void searchBluetoothDevices() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            displayNoBluetoothError();
        } else if (mBluetoothAdapter.isEnabled()) {
            List<BluetoothDevice> pairedDevices = new ArrayList<>();
            pairedDevices.addAll(mBluetoothAdapter.getBondedDevices());
            displayBluetoothDevices(pairedDevices, preferenceManager.getPrinter1Address());
        } else {
            displayEnableScreen();
        }
    }

    public void displayBluetoothDevices(List<BluetoothDevice> pairedDevices, String selected) {
        binding.lyNoBluetooth.setVisibility(View.GONE);
        binding.lyBluetoothConfig.setVisibility(View.GONE);
        binding.lySelection.setVisibility(View.VISIBLE);

        binding.rvData.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this.getApplicationContext());
        binding.rvData.addItemDecoration(new SimpleDividerItemDecoration(this.getApplicationContext(), R.drawable.line_divider_black));
        binding.rvData.setLayoutManager(layoutManager);

        BluetoothRecyclerAdapter adapter = new BluetoothRecyclerAdapter(this.getApplicationContext(), pairedDevices, selected,
                device -> {
                    setPrinter(device);
                    Toast.makeText(ConfigPrintActivity.this, "Se actualizó la impresora", Toast.LENGTH_LONG).show();
                });
        binding.rvData.setAdapter(adapter);

    }

    public void setPrinter(BluetoothDevice device) {
        preferenceManager.savePrinter1Address(device.getAddress());
    }

    public void displayNoBluetoothError() {
        binding.lyNoBluetooth.setVisibility(View.VISIBLE);
        binding.lyBluetoothConfig.setVisibility(View.GONE);
        binding.lySelection.setVisibility(View.GONE);
    }

    public void displayEnableScreen() {
        binding.lyNoBluetooth.setVisibility(View.GONE);
        binding.lyBluetoothConfig.setVisibility(View.VISIBLE);
        binding.lySelection.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.INTENT_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                searchBluetoothDevices();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //e("El Bluetooth no fue habilitado.")
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_enable:
                searchBluetoothDevices();
                break;
        }
    }
}
