package com.dms.printerzebra.util;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.dms.printerzebra.R;
import com.dms.printerzebra.interfaces.Function1;

import java.util.List;

public class BluetoothRecyclerAdapter extends RecyclerView.Adapter<BluetoothRecyclerAdapter.ViewHolder> {

    private final Function1<BluetoothDevice> function1;
    private final Context context;
    private final List<BluetoothDevice> objList;
    private String selectedAddress;

    public BluetoothRecyclerAdapter(Context context, List<BluetoothDevice> objList, String selectedAddress, Function1<BluetoothDevice> function1) {
        this.objList = objList;
        this.context = context;
        this.function1 = function1;
        this.selectedAddress = selectedAddress;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bluetooth, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final BluetoothDevice obj = objList.get(position);

        holder.tvDeviceName.setText(obj.getName());
        holder.tvDeviceAddress.setText(obj.getAddress());

        if (selectedAddress.equals(obj.getAddress())){
            holder.holder.setBackgroundColor(Color.LTGRAY);
            //(ContextCompat.getDrawable(context, R.drawable.bluebackground));
        } else {
            holder.holder.setBackgroundResource(0);
        }

        holder.holder.setOnClickListener(v -> {
            selectedAddress = obj.getAddress();
            function1.run(obj);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        return objList.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDeviceName, tvDeviceAddress;
        View holder;

        ViewHolder(View v) {
            super(v);
            tvDeviceName = v.findViewById(R.id.tvDeviceName);
            tvDeviceAddress = v.findViewById(R.id.tvDeviceAddress);
            holder = v.findViewById(R.id.holder);
        }
    }



}
