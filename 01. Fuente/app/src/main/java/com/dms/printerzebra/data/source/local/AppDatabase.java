package com.dms.printerzebra.data.source.local;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.dms.printerzebra.data.source.local.dao.TicketDao;
import com.dms.printerzebra.data.source.local.models.Ticket;

@Database(entities = { Ticket.class }, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract TicketDao ticketDao();
    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "DB_TAREO.db")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
