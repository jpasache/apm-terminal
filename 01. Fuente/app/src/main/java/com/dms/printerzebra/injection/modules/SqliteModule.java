package com.dms.printerzebra.injection.modules;

import android.content.Context;

import androidx.room.Room;

import com.dms.printerzebra.data.source.local.AppDatabase;
import com.dms.printerzebra.injection.annotations.ApplicationScope;

import dagger.Module;
import dagger.Provides;

@Module()
public class SqliteModule {

    @ApplicationScope
    @Provides
    public AppDatabase appDatabase(@ApplicationScope Context context) {
        return  AppDatabase.getDatabase(context);
    }
}
