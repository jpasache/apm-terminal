package com.dms.printerzebra.data.source;

import com.dms.printerzebra.data.source.local.models.Ticket;

import io.reactivex.Single;

public interface DataSource {
    Single<Ticket> find(int id);
    Single<Ticket> last();
    Single<Long> insert(Ticket ticket);
}